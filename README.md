# Frontline Education Code Challenge

Table of Contents
- [Challenge](Challenge)
- [Implementation Notes and Assumptions](Implementation-Notes-and-Assumptions)

## Challenge 

### Parameters
 - Should be solved in a language which demonstrates your skill for the position you have applied.
 - Deliver a working runnable solution and include a copy of the source code.
 - Write code typical of something we would be proud to have in Frontline software in production.
 - You will need to independently overcome any challenges you face in delivery. 
 - If applicable, please list your assumptions.

### Problem to Solve

```
Convert the string: 
"(id,created,employee(id,firstname,employeeType(id), lastname),location)" 
to the following output 
id
created
employee
- id
- firstname
- employeeType
-- id
- lastname
location
Bonus (output in alphabetical order):
 created
employee
- employeeType
-- id
- firstname
- id
- lastname
id
location
```

## Implementation Notes and Assumptions

- This implementation will be built primarily with JavaScript.  
  More specifically it will be built initially in node.js (v8), then added to a browser implementation for easier access and delivery on an evergreen browsers.

- **type, format, and over-all data cleanliness is irrelevant**  
  Obviously, in a real-world scenario, this wouldn't be the case, but without knowing where the data is coming from it's impossible to tell whether the data has been processed.  However, based on the "Bonus" task, the primary task is not to clean to data, so I'm assuming it's either been done prior to this or can be added at a later date

- **incoming data doesn't actually matter, it's the structure of the data that matters**  
  _**This isn't an assumption I would normally make with a challenge including words like "id", "created", "employeeType"**, etc, but after looking at both the primary and bonus tasks, it genuinely seems like data structure is what's important, so that's what I'm going to focus on._

- **testing framework is irrelevant**
  No test framework was mentioned in the challenge or job description.

- **trim spaces**  
  Any leading or trailing spaces should be trimmed by display time.  No internal spaces provided in the specified inputs, but they should be supported.

- I wish I had gone the object-based route, instead of the array-based route.


## Run tests from the command line
- `git clone git@gitlab.com:metasean/frontline-challenge.git`
- `npm install`
- `npm test`