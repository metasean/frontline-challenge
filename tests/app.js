const test = require('tape')
const app = require('../src/app.js')

test('build up to a nested array', (t) => {
  
  t.deepEquals(app.convert('just a string'), {"just a string":""})

  t.deepEquals(app.convert('string'), {"string":""})

  const actual_3 = app.convert('(stringA (stringB, stringC, string D), string a b c)')
  const expect_3 = {
    'stringA ': { 
      'stringB': '', 
      ' stringC': '', 
      ' string D': '' 
    },
    ' string a b c': ''
  }
  t.deepEqual(actual_3, expect_3)

  t.end()
})

test('convert the provided baseline example to a nested array', (t) => {
  const given = "(id,created,employee(id,firstname,employeeType(id), lastname),location)"
  const expect = {
    'id': '',
    'created': '',
    'employee': {
      'id': '',
      'firstname': '',
      'employeeType': {
        'id': '',
      },
      ' lastname': '',
    },
    'location': '',
  }

  t.deepEqual(app.convert(given), expect)
  t.end()
})

test('output the provided baseline example', (t) => {
  const given = "(id,created,employee(id,firstname,employeeType(id), lastname),location)"
  const expect = `id
created
employee
- id
- firstname
- employeeType
-- id
- lastname
location
`

  t.equal(app.baseline(given), expect)
  t.end()
})

test('output the provided bonus example', (t) => {
  const given = "(id,created,employee(id,firstname,employeeType(id), lastname),location)"
  const expect = `created
employee
- employeeType
-- id
- firstname
- id
- lastname
id
location
`

  t.equal(app.bonus(given), expect)
  t.end()
})